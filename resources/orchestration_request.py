"""Mock SO orchestration request resource."""
from dataclasses import dataclass, field
from datetime import datetime
from typing import Dict

from flask_restful import Resource


@dataclass
class OrchestrationRequestData:
    """Orchestration request dataclass."""

    request_id: str
    created_at: datetime = field(default_factory=datetime.now)


ORCHESTRATION_REQUESTS = {}


def time_diff(dt: datetime, diff: int = 1) -> bool:
    """Check if given datetime has older (in seconds) than current datetime.

    Args:
        dt (datetime): Datetime to check
        diff (int, optional): Number of seconds to check. Defaults to 1.

    Returns:
        bool: True if datetime is older, False otherwise

    """
    return (datetime.now() - dt).seconds > diff


class OrchestrationRequest(Resource):
    """Orchestration request resource."""

    @staticmethod
    def reset() -> None:
        """Reset orchestration request resource.

        Clean ORCHESTRATION_REQUESTS dictionary

        """
        global ORCHESTRATION_REQUESTS
        ORCHESTRATION_REQUESTS = {}

    def get(self, orchestration_request_id: str) -> Dict[str, Dict[str, str]]:
        """Get orchestration request data.

        Return orchestration request data from ORCHESTRATION_REQUESTS dictionary.
            If it doesn't exist it creates that.

        Args:
            orchestration_request_id (str): Orchestration request id key value

        Returns:
            Dict[str, Dict[str, str]]: Orchestration request data
        """
        try:
            orchestration_request_data = ORCHESTRATION_REQUESTS[
                orchestration_request_id
            ]
        except KeyError:
            orchestration_request_data = OrchestrationRequestData(
                request_id=orchestration_request_id
            )
            ORCHESTRATION_REQUESTS[
                orchestration_request_id
            ] = orchestration_request_data
        return {
            "request": {
                "requestStatus": {
                    "requestState": "COMPLETE"
                    if time_diff(orchestration_request_data.created_at)
                    else "IN_PROGRESS"
                }
            }
        }
